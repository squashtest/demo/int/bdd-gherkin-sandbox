# bdd-gherkin-sandbox

This sandbox toy project aims at demonstrating the Squash TM capabilities at generating and transmiting Gherkin features. Those features will be pushed by Squash TM in the src/test/resources/squash/BDD subdirectory (relative to this Readme file). Finally those generated features are not meant to be executed. Hence no actions implementations are to be found in this repository
