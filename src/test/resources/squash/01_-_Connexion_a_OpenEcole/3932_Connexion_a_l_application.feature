# language: en
Feature: Connexion à l'application

	Scenario: Connexion à l'application
		Given Je suis sur la page d'accueil de l'application
		When Je saisis mon login et mon mot de passe
		Then Je suis connecté à l'application