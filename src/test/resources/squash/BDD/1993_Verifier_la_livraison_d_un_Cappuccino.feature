# Automation priority: 2
# Test case importance: Low
# language: en
Feature: Vérifier la livraison d'un Cappuccino

	Scenario: Vérifier la livraison d'un Cappuccino
		Given la machine est en marche
		And mon solde est au moins de 0.80
		When je sélectionne le "Cappuccino"
		Then la machine me sert un "Capuccino"