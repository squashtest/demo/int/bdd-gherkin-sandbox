*** Settings ***
Documentation    Vérifier que la machine est disponible-Copie1
Metadata         ID                           3595
Metadata         Reference                    01
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Vérifier que la machine est disponible-Copie1
    [Documentation]    Vérifier que la machine est disponible-Copie1

    Given la machine est branchée
    When je passe mon badge
    Then la machine me sert un "Capuccino"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_3595_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_3595_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =         Get Variable Value    ${TEST_SETUP}
    ${TEST_3595_SETUP_VALUE} =    Get Variable Value    ${TEST_3595_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_3595_SETUP_VALUE is not None
        Run Keyword    ${TEST_3595_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_3595_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_3595_TEARDOWN}.

    ${TEST_3595_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_3595_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =         Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_3595_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_3595_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END
