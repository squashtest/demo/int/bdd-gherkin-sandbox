# language: en
Feature: Vérifier la livraison des produits

	Scenario Outline: Vérifier la livraison des produits
		Given la machine est en marche
		And mon solde est au moins de <prix>
		When je sélectionne le <produit>
		Then la machine me sert un <produit>
		And mon compte est débité de <prix>
		And mon solde est au moins de "0.80"
		And mon solde est au moins de "80"

		@Cappuccino
		Examples:
		| prix | produit |
		| 0.80 | "Cappuccino" |

		@Espresso
		Examples:
		| prix | produit |
		| 0.40 | "Espresso" |

		@Jus_de_tomate
		Examples:
		| prix | produit |
		| 1.0 | "Jus de tomate" |

		@Lungo
		Examples:
		| prix | produit |
		| 0.50 | "Lungo" |