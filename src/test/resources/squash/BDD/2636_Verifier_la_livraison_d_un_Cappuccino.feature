# language: en
Feature: Vérifier la livraison d'un Cappuccino

	Scenario: Vérifier la livraison d'un Cappuccino
		Given la machine est en marche
		And mon solde est au moins de "1.0"
		When je sélectionne le "Cappuccino"
		Then j'obtiens un "Thé"
		But j'obtiens un "Thé à la menthe"
		Given mon solde est au moins de <prix>